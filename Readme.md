# Setup do WordPress
Este setup está configurado para rodar por trás do proxy reverso [Nginx-proxy](https://gitlab.com/coletivofarpa/nginx-proxy) inclusive em ambiente localhost

## Como usar

Faça uma cópia do arquivo `.env.sample` para novo arquivo cujo nome deve ser `.env`:
``` Bash
cp .env.sample .env
```

Edite o arquivo `.env` inserindo um nome qualquer para o seu projeto em `COMPOSE_PROJECT_NAME`.
``` Bash
nano .env
```

Substitua as entradas `<dominio.aqui>` pelo endereço web do seu projeto sem `http://`. Ex.: localhost

Escolha o nome, usuário e senha do banco de dados bem como a senha do usuário root do SGDB MariaDB e rode os contêineres:
``` Bash
docker compose up -d
```

## Migração do WP para ambiente de contêineres

Crie os diretórios `volumes/wordpress/` e `volumes/mariadb/dump` caso ainda não existam:
```Bash
mkdir volumes/wordpress/ &&\
mkdir volumes/mariadb/dump/
```

Copie o diretório `wp-config/` do site de origem para o diretório `volumes/wordpress/`. 
```Bash
sudo cp -r <caminho/do/wp-content/ volumes/wordpress/
```

Coloque a cópia do DB de origem (dump) para o diretório `volumes/mariadb/dump`:
```Bash
cp <caminha/do/DUMP.sql> volumes/mariadb/dump/
```

Acerte as permisões dos diretórios `wordpress/` e `mariadb/`:
```Bash
sudo chown -R www-data:www-data volumes/wordpress/ &&\
sudo chown -R systemd-coredump:systemd-coredump volumes/mariadb/
```

Acesse o terminal Bash dentro do contêiner:
```Bash
docker exec -it <NomeDoConteinerDoBanco> bash
```
Importe o dump para o banco de dados dentro do contêiner:
```Bash
mysql -u root -p <NomeDoBanco> < /docker-entrypoint-initdb.d/<NomeDoArquivoDUMP>
```
A senha do banco será exigida. É a mesma que você adicionou ao arquivo `.env` inicialmente.

## Atualize os links no banco de dados
Acesse o banco de dados:
```Bash
mysql -u root -p <NomeDoBanco>
```
E rode o seguinte comando SQL com os devidos links de origem e destino:
```Bash
UPDATE wp_options SET option_value = REPLACE(option_value, 'https://origem.com', 'http://localhost') WHERE option_name = 'home' OR option_name = 'siteurl';
```

Caso necessário atualize os links das demais tabelas do banco de dados:
```Bash
UPDATE wp_posts SET guid = REPLACE(guid, 'https://origen.com', 'http://localhost');
```
```Bash
UPDATE wp_posts SET post_content = REPLACE(post_content, 'https://origem.com', 'http://localhost');
```
```Bash
UPDATE wp_postmeta SET meta_value = REPLACE(meta_value, 'https://origem.com', 'http://localhost');
```
## Configure a conexão do WordPress com o Redis
Abra o arquivo de configuração do WordPress `wp-config.php`
```
sudo nano volumes/wordpress/wp-config.php
```
E adicione as seguintes linhas no final do arquivo antes da linha `/* That's all, stop editing! Happy publishing. */`:
```
// Add any custom values between this line and the "stop editing" line.

define('WP_REDIS_HOST', 'redis');
define('WP_REDIS_PORT', 6379);
define('WP_REDIS_TIMEOUT', 1.5);
define('WP_REDIS_READ_TIMEOUT', 1.5);
define('WP_REDIS_DATABASE', 0);

/* That's all, stop editing! Happy publishing. */
```
O valor de WP_REDIS_HOST deve corresponder ao nome do serviço ou contêiner Redis no seu ambiente Docker.

## Configurando cabeçalhos de segurança adicionais diretamente no WordPress
No arquivo functions.php do seu tema ou em um plugin personalizado use o seguinte código:
```
function add_security_headers() {
    header('X-Content-Type-Options: nosniff');
    header('X-Frame-Options: SAMEORIGIN');
    header('X-XSS-Protection: 1; mode=block');
    header('Referrer-Policy: strict-origin-when-cross-origin');
    header('Content-Security-Policy: default-src \'self\'; script-src \'self\'; style-src \'self\'; img-src \'self\';');
    header('Strict-Transport-Security: max-age=31536000; includeSubDomains');
    header('Feature-Policy: vibrate \'none\'; geolocation \'none\';');
}
add_action('send_headers', 'add_security_headers');
```